const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = 3000;
const URL_BASE = '/apitechu/v1';
const usersFile = require('./user.json');

app.use(body_parser.json());
app.listen(port, function(){
  console.log('NodeJS escuchando en el puerto: ' + port);
});

//app.use(body_parser.json())

app.get(URL_BASE +'/users',
    function(req,res){
      res.send(usersFile);
      //console.log("jeje");
    }
);


app.post(URL_BASE +'/login',
    function(req,res){
        console.log("POST login");
        console.log(req.body.email);
        console.log(req.body.password);
        var usuario = req.body.email;
        var clave = req.body.password;
        for(user of usersFile){
            if(user.email == usuario){
                if(user.password == clave){
                  user.logged = true;
                  writeUserDataToFile(usersFile);
                  console.log("Login ok");
                  res.send({"msg" : "Login ok", "idUsuario" : user.id});
                }else{
                    console.log("Login incorrecto");
                    res.status(400).send({"msg" : "Login incorrect"});
                }
            }
        }
    }
);

app.post(URL_BASE +'/logout/:id',
    function(req,res){
      console.log("POST logout");
      var usuarioId = req.params.id;
      for(user of usersFile){
          if(user.id == usuarioId){
              if(user.logged){
                delete user.logged;
                writeUserDataToFile(usersFile);
                console.log("Logout correcto!");
                res.send({"msg" : "Logout correcto.", "idUsuario" : user.id});
              }else{
                console.log("Logout incorrecto");
                res.status(400).send({"msg" : "Logout incorrecto."});
              }
          }
      }


    }
);

function writeUserDataToFile(data){
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./user.json",jsonUserData,"utf8",
        function(error){
            if(error){
              console.log(error);
            }
        }
    );

}
